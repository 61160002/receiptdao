/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author erngeoey
 */
public class ReceiptDetail {

    private int id;
    private Product iproduct;
    private int amount;
    private double price;
    private Receipt receipt;

    public ReceiptDetail(int id, Product iproduct, int amount, double price, Receipt receipt) {
        this.id = id;
        this.iproduct = iproduct;
        this.amount = amount;
        this.price = price;
        this.receipt = receipt;
    }

    public ReceiptDetail(Product product, int amount, double price, Receipt receipt) {
        this(-1, product, amount, price, receipt);
    }

    public double getTotal() {
        return amount * price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getIproduct() {
        return iproduct;
    }

    public void setIproduct(Product iproduct) {
        this.iproduct = iproduct;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }
    
    public void addAmount(int amount){
       this.amount = this.amount+amount;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id
                + ", product=" + iproduct 
                + ", amount=" + amount
                + ", price=" + price
                + ", total=" + this.getTotal()
                + '}';
    }

}
